# simple_ws

This project is a workspace that has the goal to provide you simple node that can be used as a template during the development of your project.

## Getting Started

In order to compile it you have to open the terminal and enter in the simple_ws forlder.
Suppose to put simple_ws inside your home, you have to type

```
cd && cd simple_ws
```

Now you have to it compile with

```
catkin_make
```

## Authors

* **Davide Ferrari** 
* **Andrea Pupa** 
